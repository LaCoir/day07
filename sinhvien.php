<?php
/**
 * Export to PHP Array plugin for PHPMyAdmin
 * @version 5.2.0
 */

/**
 * Database `sinhvien`
 */

/* `sinhvien`.`persons` */
$persons = array(
  array('No' => '1','TenSV' => 'Nguyen Van A','Khoa' => 'Khoa hoc may tinh'),
  array('No' => '2','TenSV' => 'Nguyen Thi B','Khoa' => 'Khoa hoc vat lieu')
);
